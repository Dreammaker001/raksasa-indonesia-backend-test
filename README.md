Backend test Raksasa Indonesia, API pemesanan makanan pada aplikasi kada

Laravel 8

## Cara Menjalankan
<ol>
  <li>Jalankan <strong>composer install</strong> di cmd atau terminal Anda</li>
  <li>copy paste file .env.example dan rename menjadi .env di folder root.</li>
  <li>Buka file .env Anda dan ubah nama database (DB_DATABASE) menjadi apa pun yang Anda miliki, bidang nama pengguna (DB_USERNAME) dan kata sandi (DB_PASSWORD) sesuai dengan konfigurasi Anda.</li>
  <li>Jalankan <strong>php artisan key:generate</strong> </li>
  <li>Jalankan <strong>php artisan migrate --seed</strong> </li>
  <li>Jalankan <strong>php artisan storage:link</strong> untuk mengakses file gambar yang sudah diupload</li>
  <li>Jalankan <strong>php artisan serve</strong> untuk menjalankan server </li>
  <!-- <li>Jalankan <strong>php artisan test</strong> untuk menjalankan mode Test </li> -->
</ol>

## Dokumentasi API Postman
Import file <strong>raksasa-indonesia-test.postman_collection.json</strong> ke dalam aplikasi postman anda untuk mencoba API dari project ini
