<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image');
            $table->unsignedBigInteger('price')->default(0);
            $table->text('description')->default("");
            $table->unsignedBigInteger('sold')->default(0);
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('outlet_id');
            $table->foreign('category_id')->references('id')->on('food_categories')->onUpdate('cascade');
            $table->foreign('outlet_id')->references('id')->on('outlets')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods');
    }
}
