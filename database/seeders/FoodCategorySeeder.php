<?php

namespace Database\Seeders;

use App\Models\FoodCategory;
use Illuminate\Database\Seeder;

class FoodCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $food_category = new FoodCategory();
        $food_category->name = "chicken";
        $food_category->save();

        $food_category = new FoodCategory();
        $food_category->name = "coffee";
        $food_category->save();

        $food_category = new FoodCategory();
        $food_category->name = "meat";
        $food_category->save();

        $food_category = new FoodCategory();
        $food_category->name = "rice";
        $food_category->save();

        $food_category = new FoodCategory();
        $food_category->name = "vegetable";
        $food_category->save();

        $food_category = new FoodCategory();
        $food_category->name = "snack";
        $food_category->save();
    }
}
