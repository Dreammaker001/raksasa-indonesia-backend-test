<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'foods';

    protected $fillable = [
        'name',
        'image',
        'price',
        'description',
        'sold',
        'category_id',
        'outlet_id'
    ];

    public function productCart () {
        return $this->hasOne(ProductCart::class, 'food_id', 'id');
    }
}
