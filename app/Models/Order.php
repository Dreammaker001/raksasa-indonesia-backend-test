<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'outlet_id',
        'payment_status',
        'grand_total'
    ];

    public function orderFoods () {
        return $this->hasMany(OrderFood::class, 'order_id', 'id');
    }
}
