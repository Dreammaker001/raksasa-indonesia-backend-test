<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'outlet_id',
    ];

    public function productCart () {
        return $this->hasMany(ProductCart::class, 'cart_id', 'id');
    }
}
