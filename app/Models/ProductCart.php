<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCart extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'food_id',
        'qty'
    ];

    public function cart () {
        return $this->belongsTo(Cart::class, 'cart_id', 'id');
    }

    public function food () {
        return $this->belongsTo(Food::class, 'food_id', 'id');
    }
}
