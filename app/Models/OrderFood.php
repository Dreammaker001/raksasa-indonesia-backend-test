<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderFood extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'food_id',
        'qty'
    ];

    public function order () {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function food () {
        return $this->belongsTo(Food::class, 'food_id', 'id');
    }
}
