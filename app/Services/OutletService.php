<?php
namespace App\Services;

use App\Models\Outlet;
use Illuminate\Support\Facades\Storage;

class OutletService{
    public function store($data) {
        $user_id = auth()->user()->id;
        
        $logo = $data->file('logo');
        $banner = $data->file('banner');
        if($logo) {
            $logoPath = $logo->store('outlets', 'public');
        }
        
        if($banner) {
            $bannerPath = $banner->store('outlets/banner', 'public');
        }
        $create = Outlet::create([
            'name' => $data->name,
            'logo' => $logoPath,
            'banner' => $bannerPath,
            'operational_hour' => $data->operational_hour,
            'user_id' => $user_id
        ]);

        return $create;
    }

    public function update ($id, $data) {
        $user_id = auth()->user()->id;
        $update = Outlet::where([
            ['id', '=', $id],
            ['user_id', '=', $user_id]
        ])->first();
        $logo = $data->file('logo');
        $banner = $data->file('banner');

        $update->name = $data->name ? $data->name : $update->name;
        $update->operational_hour = $data->operational_hour ? $data->operational_hour : $update->operational_hour;

        if($logo) {
            if($update->logo && file_exists(storage_path('app/public/'.$update->logo))){
                Storage::delete('public/'.$update->logo);
            }
            $imgPath = $logo->store('outlets', 'public');
            $update->logo = $imgPath;
        }

        if($banner) {
            if($update->banner && file_exists(storage_path('app/public/'.$update->banner))){
                Storage::delete('public/'.$update->banner);
            }
            $imgPath = $banner->store('outlets/banner', 'public');
            $update->banner = $imgPath;
        }

        $update->save();

        return $update;
    }

    public function delete ($id) {
        $user_id = auth()->user()->id;
        $delete = Outlet::where([
            ['id', '=', $id],
            ['user_id', '=', $user_id]
        ])->first();

        if($delete->logo && file_exists(storage_path('app/public/'.$delete->logo))){
            Storage::delete('public/'.$delete->logo);
        }

        if($delete->banner && file_exists(storage_path('app/public/'.$delete->banner))){
            Storage::delete('public/'.$delete->banner);
        }

        $delete->delete();

        return $delete;
    }
}