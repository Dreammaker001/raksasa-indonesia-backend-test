<?php
namespace App\Services;

use App\Models\Food;
use Illuminate\Support\Facades\Storage;

class FoodService {
    public function store ($data) {
        $image = $data->file('image');
        if($image) {
            $imagePath = $image->store('foods', 'public');
        }

        $store = Food::create([
            'name' => $data->name,
            'image' => $imagePath,
            'price' => $data->price,
            'description' => $data->description ? $data->description : '',
            'sold' => $data->sold ? $data->sold : 0,
            'category_id' => $data->category_id,
            'outlet_id' => $data->outlet_id
        ]);

        return $store;
    }

    public function update ($id, $data) {
        $update = Food::find($id);
        $image = $data->file('image');
        if($image) {
            if($update->image && file_exists(storage_path('app/public/'.$update->image))){
                Storage::delete('public/'.$update->image);
            }
            $imgPath = $image->store('foods', 'public');
            $update->image = $imgPath;
        }

        $update->name = $data->name ? $data->name : $update->name;
        $update->price = $data->price ? $data->price : $update->price;
        $update->description = $data->description ? $data->description : $update->description;
        $update->sold = $data->sold ? $data->sold : $update->sold;
        $update->category_id = $data->category_id ? $data->category_id : $update->category_id;
        $update->outlet_id = $data->outlet_id ? $data->outlet_id : $update->outlet_id;
        $update->save();

        return $update;
    }

    public function delete ($id) {
        $delete = Food::find($id);
        if($delete->image && file_exists(storage_path('app/public/'.$delete->image))){
            Storage::delete('public/'.$delete->image);
        }
        $delete->delete();

        return $delete;
    }
}