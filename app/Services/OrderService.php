<?php
namespace App\Services;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderFood;
use App\Models\ProductCart;
use Illuminate\Support\Facades\DB;

class OrderService {
    public function index () {
        $user_id = auth()->user()->id;

        $data = Order::where('user_id', $user_id)
        ->with(['orderFoods' => function ($q) {
            $q->join('foods', 'food_id', '=', 'foods.id')
            ->select('order_food.*', DB::raw("(qty * foods.price) as sub_total"))
            ->whereColumn('food_id', 'foods.id');
        },
        'orderFoods.food'])
        ->join('order_food', 'orders.id', '=', 'order_food.order_id')
        ->join('foods', 'order_food.food_id', '=', 'foods.id')
        ->select('orders.*', DB::raw("SUM(order_food.qty * foods.price ) as grand_total"))
        ->groupBy('orders.id')
        ->groupBy('orders.user_id')
        ->groupBy('orders.outlet_id')
        ->groupBy('orders.payment_status')
        ->groupBy('orders.created_at')
        ->groupBy('orders.updated_at')
        ->get();

        return $data;
    }

    public function store ($data) {
        $user_id = auth()->user()->id;
        $cart = Cart::find($data->cart_id);

        $order = Order::create([
            'user_id' => $user_id,
            'outlet_id' => $cart->outlet_id,
            'payment_status' => $data->payment_status,
        ]);

        $arr = [];
        foreach ($cart->productCart as $key => $value) {
            $arr[] = [
                'order_id' => $order->id,
                'food_id' => $value->food_id,
                'qty' => $value->qty
            ];
        }

        OrderFood::insert($arr);
        ProductCart::where('cart_id', $cart->id)->delete();
        $cart->delete();

        return $this->index();
    }

    public function show ($id) {
        $user_id = auth()->user()->id;
        $data = Order::where([
            ['orders.id', '=', $id],
            ['orders.user_id', '=', $user_id]
        ])
        ->with(['orderFoods' => function ($q) {
            $q->join('foods', 'food_id', '=', 'foods.id')
            ->select('order_food.*', DB::raw("(qty * foods.price) as sub_total"))
            ->whereColumn('food_id', 'foods.id');
        },
        'orderFoods.food'])
        ->join('order_food', 'orders.id', '=', 'order_food.order_id')
        ->join('foods', 'order_food.food_id', '=', 'foods.id')
        ->select('orders.*', DB::raw("SUM(order_food.qty * foods.price ) as grand_total"))
        ->groupBy('orders.id')
        ->groupBy('orders.user_id')
        ->groupBy('orders.outlet_id')
        ->groupBy('orders.payment_status')
        ->groupBy('orders.created_at')
        ->groupBy('orders.updated_at')
        ->get();

        if(!$data) return null;

        return $data[0];
    }

    public function update ($id, $data) {
        $user_id = auth()->user()->id;
        $update = Order::where([
            ['id', '=', $id],
            ['user_id', '=', $user_id]
        ])->first();

        if(!$update) return null;

        $update->payment_status = $data->payment_status;
        $update->save();


        return $update;
    }
}