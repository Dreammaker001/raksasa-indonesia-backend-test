<?php
namespace App\Services;

use App\Models\FoodCategory;

class FoodCategoryService {
    public function store ($data) {
        $store = FoodCategory::create($data->all());

        return $store;
    }

    public function update ($id, $data) {
        $update = FoodCategory::find($id);
        $update->name = $data->name;
        $update->save();

        return $update;
    }

    public function delete ($id) {
        $delete = FoodCategory::find($id);
        $delete->delete();

        return $delete;
    }
}