<?php
namespace App\Services;

use App\Models\Cart;
use App\Models\Food;
use App\Models\ProductCart;
use Illuminate\Support\Facades\DB;

class CartService {
    public function index () {
        $user_id = auth()->user()->id;

        $data = Cart::where('user_id', $user_id)
        ->with(['productCart' => function ($q) {
            $q->join('foods', 'food_id', '=', 'foods.id')
            ->select('product_carts.*', DB::raw("(qty * foods.price) as sub_total"))
            ->whereColumn('food_id', 'foods.id');
        },
        'productCart.food'])
        ->join('product_carts', 'carts.id', '=', 'product_carts.cart_id')
        ->join('foods', 'product_carts.food_id', '=', 'foods.id')
        ->select('carts.*', DB::raw("SUM(product_carts.qty * foods.price ) as total"))
        ->groupBy('carts.id')
        ->groupBy('carts.user_id')
        ->groupBy('carts.outlet_id')
        ->groupBy('carts.created_at')
        ->groupBy('carts.updated_at')
        ->get();

        return $data;
    }

    public function store ($data) {
        $user_id = auth()->user()->id;
        $food = Food::find($data->food_id);

        if(!$food) {
            return $this->index();
        }

        $check = Cart::where([
            ['outlet_id', '=', $food->outlet_id],
            ['user_id', '=', $user_id]
        ])->first();
        if(!$check) {
            $storeCart = Cart::create([
                'user_id' => $user_id,
                'outlet_id' => $food->outlet_id
            ]);
            $storeProductCart = ProductCart::create([
                'cart_id' => $storeCart->id,
                'food_id' => $food->id,
                'qty' => $data->qty
            ]);
        } else {
            $storeProductCart = ProductCart::create([
                'cart_id' => $check->id,
                'food_id' => $food->id,
                'qty' => $data->qty
            ]);
        }


        return $this->index();
    }

    public function update ($id, $data) {
        $user_id = auth()->user()->id;
        $update = ProductCart::find($id);

        if($update->cart->user_id != $user_id){
            return null;
        }

        $update->qty = $data->qty;
        $update->save();

        return $this->index();
    }

    public function delete ($id) {
        $user_id = auth()->user()->id;
        $productCart = ProductCart::find($id);

        if($productCart->cart->user_id != $user_id){
            return null;
        }

        $productCart->delete();
        $cart = Cart::find($productCart->cart_id);

        if(count($cart->productCart) < 1) {
            $cart->delete();
        }

        return $this->index();
    }
}