<?php
namespace App\Services;

use App\Helpers\ApiFormatter;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;

class AuthService {
    public function register ($data) {
        $user = User::create([
            'name' => $data->name,
            'phone_number' => $data->phone_number,
            'password' => bcrypt($data->password)
        ]);

        $token = $user->createToken($data->phone_number)->plainTextToken;

        return [
            'user' => $user,
            'token' => $token
        ];
    }

    public function login ($data) {
        $user = User::where('phone_number', $data->phone_number)->first();
        if(!$user || !Hash::check($data->password, $user->password)) {
            return ApiFormatter::errorResponse( 401, 'fail', 'Bad Credentials');
        }

        $token = $user->createToken($data->phone_number)->plainTextToken;

        return ApiFormatter::successResponse(200, 'success', [
            'user' => $user,
            'token' => $token
        ]);
    }

    public function logout () {
        $id = auth()->user()->id;
        PersonalAccessToken::where('tokenable_id', $id)->delete();
        return null;
    }
}