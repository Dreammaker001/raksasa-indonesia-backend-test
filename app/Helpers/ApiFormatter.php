<?php

namespace App\Helpers;

class ApiFormatter {

    public static function customResponse ($code = null, $response) {
        return response()->json($response, $code);
    }

    public static function successResponse ($code = null, $status = null, $data = null) {
        $response = [
            "status" => $status,
            "data" => $data
        ];
        return response()->json($response, $code);
    }

    public static function errorResponse ($code = null, $status = null, $message = null) {
        $response = [
            "status" => $status,
            "message" => $message
        ];
        return response()->json($response, $code);
    }
}