<?php

namespace App\Http\Requests\Food;

use App\Helpers\ApiFormatter;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateFoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string'],
            'image' => ['image'],
            'stock' => ['numeric'],
            'description'=> ['string'],
            'sold' => ['numeric'],
            'category_id' => ['numeric', 'exists:App\Models\FoodCategory,id'],
            'outlet_id' => ['numeric', 'exists:App\Models\Outlet,id']
        ];
    }

    public function failedValidation (Validator $validator) {
        throw new HttpResponseException(
            ApiFormatter::errorResponse('400', 'fail', $validator->errors())
        );
    }
}
