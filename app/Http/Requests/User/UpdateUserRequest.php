<?php

namespace App\Http\Requests\User;

use App\Helpers\ApiFormatter;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string'],
            'password' => ['confirmed', 'string']
        ];
    }

    public function failedValidation (Validator $validator) {
        throw new HttpResponseException(
            ApiFormatter::errorResponse('400', 'fail', $validator->errors())
        );
    }
}
