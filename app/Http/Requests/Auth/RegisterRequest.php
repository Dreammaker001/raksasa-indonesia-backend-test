<?php

namespace App\Http\Requests\Auth;

use App\Helpers\ApiFormatter;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'phone_number' => ['required', 'numeric', 'unique:App\Models\User,phone_number'],
            'password' => ['required', 'confirmed', 'string']
        ];
    }

    public function failedValidation (Validator $validator) {
        throw new HttpResponseException(
            ApiFormatter::errorResponse('400', 'fail', $validator->errors())
        );
    }
}
