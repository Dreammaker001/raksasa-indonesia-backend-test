<?php

namespace App\Http\Requests\Cart;

use App\Helpers\ApiFormatter;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreProductCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'food_id' => ['required', 'numeric', 'exists:App\Models\Food,id'],
            'qty' => ['required', 'numeric'],
        ];
    }

    public function failedValidation (Validator $validator) {
        throw new HttpResponseException(
            ApiFormatter::errorResponse('400', 'fail', $validator->errors())
        );
    }
}
