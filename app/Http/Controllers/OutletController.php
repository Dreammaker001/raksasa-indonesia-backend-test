<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Http\Requests\Outlet\CreateOutletRequest;
use App\Http\Requests\Outlet\UpdateOutletRequest;
use App\Models\Outlet;
use App\Services\OutletService;
use Illuminate\Http\Request;

class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Outlet::all();

        return ApiFormatter::successResponse(200, "success", $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        CreateOutletRequest $request,
        OutletService $outletService
    )
    {
        $create = $outletService->store($request);
        return ApiFormatter::successResponse(201, 'success', $create);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        UpdateOutletRequest $request, 
        OutletService $outletService,
        $id
    )
    {
        $update = $outletService->update($id, $request);

        return ApiFormatter::successResponse(200, 'success', $update);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        OutletService $outletService,
        $id
    )
    {
        $delete = $outletService->delete($id);
        
        return ApiFormatter::successResponse(200, 'success', $delete);
    }
}
