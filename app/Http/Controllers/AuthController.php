<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use App\Services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create user account.
     *
     * @return \Illuminate\Http\Response
     */
    public function register (
        RegisterRequest $request,
        AuthService $authService
    ) {
        $register = $authService->register($request);

        return ApiFormatter::successResponse(200, 'success', $register);
    }

    public function login (
        LoginRequest $request,
        AuthService $authService
    ) {
       return $authService->login($request);
    }

    public function logout (
        AuthService $authService
    ) {
        $logout = $authService->logout();

        return ApiFormatter::successResponse(200, 'success', $logout);
    }
}
