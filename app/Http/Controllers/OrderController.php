<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Http\Requests\Order\StoreOrderRequest;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(
        OrderService $orderService
    )
    {
        $data = $orderService->index();

        return ApiFormatter::successResponse(200, 'success', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        StoreOrderRequest $request,
        OrderService $orderService
    )
    {
        $store = $orderService->store($request);

        return ApiFormatter::successResponse(201, 'success', $store);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(
        OrderService $orderService,
        $id
    )
    {
        $data = $orderService->show($id);

        return ApiFormatter::successResponse(200, 'success', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        UpdateOrderRequest $request, 
        OrderService $orderService,
        $id
    )
    {
        $data = $orderService->update($id, $request);
        
        return ApiFormatter::successResponse(200, 'success', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
