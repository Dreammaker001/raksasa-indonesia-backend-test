<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Http\Requests\FoodCategory\StoreFoodCategoryRequest;
use App\Http\Requests\FoodCategory\UpdateFoodCategoryRequest;
use App\Models\FoodCategory;
use App\Services\FoodCategoryService;
use Illuminate\Http\Request;

class FoodCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FoodCategory::all();

        return ApiFormatter::successResponse(200, 'success', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        StoreFoodCategoryRequest $request,
        FoodCategoryService $foodCategoryService
    )
    {
        $store = $foodCategoryService->store($request);

        return ApiFormatter::successResponse(201, 'success', $store);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        UpdateFoodCategoryRequest $request,
        FoodCategoryService $foodCategoryService,
        $id
    )
    {
        $update = $foodCategoryService->update($id, $request);

        return ApiFormatter::successResponse(200, 'success', $update);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        FoodCategoryService $foodCategoryService,
        $id
    )
    {
        $delete = $foodCategoryService->delete($id);
        
        return ApiFormatter::successResponse(200, 'success', $delete);
    }
}
